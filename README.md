# Energy Oracle

First, make a user on the homeserver whose identity the bot will assume, [eg](https://matrix-org.github.io/synapse/latest/setup/installation.html?highlight=register_#registering-a-user)
```sh
register_new_matrix_user --user "alice" --password "p455w0rd" --config /etc/matrix-synapse/conf.d/registration.yaml
```

```sh
# either
# Debian
sudo apt update
sudo apt install python3 python3-pip python3-venv libolm3
# Fedora
sudo dnf install python3 libolm
# end

# if pdm is not installed (this installation method ensures latest version)
curl -sSL https://pdm-project.org/install-pdm.py | python3 -
# else (unless installed by distro package)
pdm self update
# end

cp template.config.toml config.toml
# populate config.toml with the user details

pdm install
pdm start
```

When invited to a room, the bot should join automatically.

Example systemd profile `/etc/systemd/system/energy-oracle.service`:

```systemd
[Unit]
Description=Energy Oracle
After=network.target

[Service]
User=<user>
Group=<user>

WorkingDirectory=/home/<user>/git/matrix-bot-dynamic-energy-prices
ExecStart=/home/<user>/.local/bin/pdm start

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
