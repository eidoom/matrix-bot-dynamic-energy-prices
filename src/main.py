#!/usr/bin/env python3

import asyncio
import calendar
import datetime
import logging
import pathlib
import sqlite3
import time
import tomllib

from aiohttp import (
    ClientTimeout,
    ClientSession,
)
from nio import (
    AsyncClient,
    InviteMemberEvent,
    MatrixRoom,
    RoomMessageText,
    AsyncClientConfig,
)

logging.basicConfig(
    level=logging.INFO,
    format="{asctime} - {name} - {levelname} - {message}",
    style="{",
    datefmt="%Y-%m-%d %H:%M:%S",
)


def get_cfg(filename):
    cfg_file = pathlib.Path(filename)

    with open(cfg_file, "rb") as f:
        cfg = tomllib.load(f)

    matrix = cfg["matrix"]
    matrix["address"] = f"https://{matrix['subdomain']}.{matrix['domain']}"
    matrix["user_id"] = f"@{matrix['username']}:{matrix['domain']}"

    return matrix, cfg["octopus"]


async def send_message(client: AsyncClient, room, msg):
    await client.room_send(
        room_id=room,
        message_type="m.room.message",
        content={"msgtype": "m.text", "body": msg},
    )


class OutOfBoundsError(Exception):
    def __init__(self, n, i):
        self.n = n
        self.i = i


class Cache:
    def __init__(self, store):
        self.con = sqlite3.connect(
            f"{store}/cache.db", detect_types=sqlite3.PARSE_COLNAMES
        )
        self.cur = self.con.cursor()

        with open("cache_schema.sql", "r") as f:
            self.cur.executescript(f.read())

    @staticmethod
    def dict_factory(cursor, row):
        fields = [column[0] for column in cursor.description]
        return {key: value for key, value in zip(fields, row)}

    @staticmethod
    def pluck_factory(_, row):
        return row[0]

    def insert(self, dics, time):
        logging.info("Updating cache")
        self.cur.executemany(
            """
            INSERT INTO unit_rate (time, price)
            VALUES (:time, :price)
            ON CONFLICT (time)
            DO UPDATE SET price = :price
            """,
            dics,
        )
        self.cur.execute("DELETE FROM mod_time")
        self.cur.execute("INSERT INTO mod_time (time) VALUES (?)", (time,))
        self.con.commit()

    def clean(self, cut):
        logging.info(f"Removing cache entries from before {cut}")
        self.cur.execute("DELETE FROM unit_rate WHERE time < ?", (cut,))
        self.con.commit()

    def last_update(self):
        self.cur.row_factory = self.pluck_factory
        time = self.cur.execute('SELECT time "[timestamp]" FROM mod_time').fetchone()
        logging.info(f"Last cache update was at {time}")
        return time

    def get(self):
        logging.info("Retrieving cache")
        self.cur.row_factory = self.dict_factory
        return self.cur.execute(
            'SELECT time AS "time [timestamp]", price FROM unit_rate ORDER BY time'
        ).fetchall()

    def current(self):
        logging.info("Fetching current price")
        self.cur.row_factory = self.pluck_factory
        return self.cur.execute(
            "SELECT price FROM unit_rate ORDER BY time LIMIT 1"
        ).fetchone()


class Octopus:
    def __init__(self, cfg_oct, store_name):
        product_code = cfg_oct["product_code"]
        region = cfg_oct["region"]
        register = cfg_oct["register"]
        self.url = f"https://api.octopus.energy/v1/products/{product_code}/electricity-tariffs/{register}-{product_code}-{region}/standard-unit-rates/"
        self.timeout = ClientTimeout(total=cfg_oct["timeout"])
        self.pub_time = datetime.time(**cfg_oct["publish"])
        self.cache = Cache(store_name)

    @staticmethod
    def now():
        return datetime.datetime.now(datetime.timezone.utc)

    @staticmethod
    def zulu(dt):
        return dt.isoformat(timespec="minutes").replace("+00:00", "Z")

    def format_time(self, dt):
        fstr = f"%H:%M {'tomorrow' if self.now().day != dt.day else 'today'}"
        return dt.strftime(fstr)

    @staticmethod
    def format_timedelta(td):
        all_minutes, seconds = divmod(int(td.total_seconds()), 60)
        hours, minutes = divmod(all_minutes, 60)
        return (f"{hours} hours and " if hours else "") + f"{minutes} minutes"

    @staticmethod
    def format_price(n):
        return f"{n:.1f}p"

    @staticmethod
    def next_day(timepoint):
        _, last_day_of_month = calendar.monthrange(timepoint.year, timepoint.month)

        if timepoint.day == last_day_of_month:
            if timepoint.month == 12:
                return timepoint.replace(
                    year=timepoint.year + 1,
                    month=1,
                    day=1,
                )
            else:
                return timepoint.replace(
                    month=timepoint.month + 1,
                    day=1,
                )
        else:
            return timepoint.replace(
                day=timepoint.day + 1,
            )

    def next_pub(self, timepoint):
        today_pub = timepoint.replace(
            hour=self.pub_time.hour,
            minute=self.pub_time.minute,
        )
        next_time = (
            self.next_day(today_pub) if timepoint.time() > self.pub_time else today_pub
        )
        logging.info(f"Next publication after {timepoint} is at {next_time}")
        return next_time

    async def pull(self):
        logging.info("Getting Octopus data")

        now = self.now()

        self.cache.clean(now - datetime.timedelta(minutes=30))

        cache_time = self.cache.last_update()

        if cache_time is None or now > self.next_pub(cache_time):
            logging.info("Cache is out of date")
            logging.info("Querying Octopus for prices")

            url = f"{self.url}?period_from={self.zulu(now)}"
            results = []

            async with ClientSession() as session:
                while url:
                    async with session.get(url, timeout=self.timeout) as resp:
                        output = await resp.json()

                    results += output["results"]
                    url = output["next"]

            cache_data = [
                {
                    "time": datetime.datetime.fromisoformat(slot["valid_from"]),
                    "price": slot["value_inc_vat"],
                }
                for slot in results
            ]

            self.cache.insert(cache_data, now)

        else:
            logging.info("Cache is up to date")

    async def summary(self):
        await self.pull()
        data = self.cache.get()

        end = max(x["time"] for x in data) + datetime.timedelta(minutes=30)

        trough = min(data, key=lambda x: x["price"])
        peak = max(data, key=lambda x: x["price"])

        average = sum(x["price"] for x in data) / len(data)

        return (
            f"The average price until {self.format_time(end)} is {self.format_price(average)}, "
            f"with a minimum of {self.format_price(trough['price'])} at {self.format_time(trough['time'])} "
            f"and a maximum of {self.format_price(peak['price'])} at {self.format_time(peak['time'])}."
        )

    async def current(self):
        await self.pull()
        data = self.cache.current()
        return f"The current unit rate is {self.format_price(data)}."

    async def best(self, hours):
        await self.pull()
        data = self.cache.get()

        n = len(data)
        slots = int(round(hours * 2, 0))
        if slots > n:
            raise OutOfBoundsError(n, slots)

        times = [x["price"] for x in data]
        blocks = [sum(times[i : i + slots]) for i in range(n - slots)]

        cheapest = min(blocks)
        i = blocks.index(cheapest)
        cheapest /= slots
        time = data[i]["time"]
        gap = time - self.now()

        return (
            f"The best {hours} hour ({slots} slot) period starts at {self.format_time(time)}, "
            f"which is {self.format_timedelta(gap)} from now, "
            f"with an average unit rate of {self.format_price(cheapest)}."
        )


class Callbacks:
    def __init__(self, client: AsyncClient, cfg_mat, cfg_oct, octo):
        self.client = client
        self.cfg_mat = cfg_mat
        self.cfg_oct = cfg_oct
        self.octo = octo

    async def message(self, room: MatrixRoom, event: RoomMessageText) -> None:
        if event.sender == self.client.user:
            return

        logging.info("Message received")

        rid = room.room_id
        await self.client.room_typing(rid)
        msg = event.body.strip().split(" ")
        cmd = msg[0].lower()
        args = msg[1:]

        match cmd:
            case "best":
                try:
                    hours = float(args[0])
                    rtn = await self.octo.best(hours)
                except IndexError:
                    rtn = (
                        "How many hours? "
                        'For example, to get the best 2.5 hour period, message "best 2.5".'
                    )
                except ValueError:
                    rtn = (
                        "Give me a number written in digits for how many hours. "
                        'For example, to get the best 2.5 hour period, message "best 2.5".'
                    )
                except OutOfBoundsError as error:
                    rtn = (
                        f"{hours} hours is {error.i} slots, "
                        f"but I only know about the next {error.n} slots, "
                        f"which is {error.n/2} hours. "
                        "Choose a smaller number of hours."
                    )
            case "help":
                rtn = "commands: " + ", ".join(
                    [
                        "best n",
                        "help",
                        "link",
                        "now",
                        "prices",
                        "time",
                    ]
                )
            case "link":
                rtn = self.cfg_oct["link"]
            case "now":
                rtn = await self.octo.current()
            case "prices":
                rtn = await self.octo.summary()
            case "time":
                now = datetime.datetime.now()
                rtn = now.strftime("It's %H:%M on %a %d %b %Y.")
            case _:
                rtn = (
                    "Sorry, I don't know that command. "
                    'Message "help" to see my list of commands.'
                )

        await self.client.room_typing(rid, False)
        await send_message(self.client, rid, rtn)

    async def invite(self, room: MatrixRoom, event: InviteMemberEvent) -> None:
        if event.state_key == self.client.user_id and (
            self.cfg_mat.federated
            or event.sender.split(":")[1] == self.cfg_mat["domain"]
        ):
            await self.client.join(room.room_id)


class Bot:
    def __init__(self, cfg_name="config.toml", store_name="store"):
        self.cfg_mat, self.cfg_oct = get_cfg(cfg_name)

        store = pathlib.Path(store_name)
        store.mkdir(exist_ok=True)

        client_config = AsyncClientConfig(
            store_sync_tokens=True,
            request_timeout=self.cfg_mat["timeout"],
        )

        self.client = AsyncClient(
            self.cfg_mat["address"],
            self.cfg_mat["user_id"],
            device_id=self.cfg_mat["device_id"],
            store_path=store,
            config=client_config,
        )

        self.octo = Octopus(self.cfg_oct, store_name)

        callbacks = Callbacks(self.client, self.cfg_mat, self.cfg_oct, self.octo)

        self.client.add_event_callback(callbacks.message, RoomMessageText)

        self.client.add_event_callback(callbacks.invite, InviteMemberEvent)

    async def login(self):
        try:
            self.client.access_token = self.cfg_mat["access_token"]
            self.client.user_id = self.cfg_mat["user_id"]
            self.client.load_store()
            logging.info(
                f"Logged in as {self.client.user} using access token, "
                f"device id: {self.client.device_id}."
            )

        except AttributeError:
            response = await self.client.login(
                password=self.cfg_mat["password"],
                device_name=self.cfg_mat["device_id"],
            )
            access_token = response.access_token
            logging.info(response)

            print("Copy next line to config.toml")
            print(f'access_token = "{access_token}"')

    async def syncer(self):
        await self.client.sync_forever(full_state=True)

    async def daily_scheduler(self):
        while True:
            now = self.octo.now()
            next_schedule = self.octo.next_pub(now)
            time_to_wait = (next_schedule - now).total_seconds()

            await asyncio.sleep(time_to_wait)

            rooms = (await self.client.joined_rooms()).rooms

            for room in rooms:
                await self.client.room_typing(room)

            msg = (
                f"It's time for the next day-ahead prices! {await self.octo.summary()}"
            )

            for room in rooms:
                await self.client.room_typing(room, False)
                await send_message(self.client, room, msg)


async def main() -> None:
    bot = Bot()

    await bot.login()

    async with asyncio.TaskGroup() as tg:
        tg.create_task(bot.syncer())
        tg.create_task(bot.daily_scheduler())


def adapt_datetime_epoch(val: datetime.datetime) -> int:
    return int(val.timestamp())


def convert_timestamp(val: str) -> datetime.datetime:
    return datetime.datetime.fromtimestamp(int(val), datetime.timezone.utc)


if __name__ == "__main__":
    sqlite3.register_adapter(datetime.datetime, adapt_datetime_epoch)
    sqlite3.register_converter("timestamp", convert_timestamp)
    asyncio.run(main())
